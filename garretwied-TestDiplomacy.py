#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    
    def test_read_1(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a[0][0], "A")
        self.assertEqual(a[0][1], "Madrid")
        self.assertEqual(a[0][2], "Hold")
        
    def test_read_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i[0],  "A")
        self.assertEqual(i[1],  "Madrid")
        self.assertEqual(i[2],  "Hold")
        self.assertEqual(j[0],  "B")
        self.assertEqual(j[1],  "Barcelona")
        self.assertEqual(j[2],  "Move")
        self.assertEqual(j[3],  "Madrid")
        self.assertEqual(k[0],  "C")
        self.assertEqual(k[1],  "London")
        self.assertEqual(k[2],  "Support")
        self.assertEqual(k[3],  "B")
    
    def test_read_3(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\n"
        i, j = diplomacy_read(s)
        self.assertEqual(i[0],  "A")
        self.assertEqual(i[1], "Madrid")
        self.assertEqual(i[2], "Hold")
        self.assertEqual(j[0],  "B")
        self.assertEqual(j[1], "Barcelona")
        self.assertEqual(j[2], "Move")
        self.assertEqual(j[3], "Madrid")
    
    #Check if no amry has most support
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual( w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    #Out of order test
    def test_solve2(self):
        r = StringIO("B Barcelona Move Madrid\nA Madrid Hold\nD Paris Support B\nC London Move Madrid\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual( w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
        
    #One line   
    def test_solve3(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual( w.getvalue(), "A Madrid\n")
    
    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual( w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    # ----
    # eval
    # ----
'''
    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    
    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
    
    def test_eval_6(self):
        v = collatz_eval(2, 2)
        self.assertEqual(v, 2)
    
    def test_eval_7(self):
        v = collatz_eval(30, 10)
        self.assertEqual(v, 112)
        
    def test_eval_8(self):
        v = collatz_eval(1, 1000)
        self.assertEqual(v, 179)
        
    def test_eval_9(self):
        v = collatz_eval(3100, 3500)
        self.assertEqual(v, 199)
        
    def test_eval_10(self):
        v = collatz_eval(1500, 6500)
        self.assertEqual(v, 262)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 2, 2, 2)
        self.assertEqual(w.getvalue(), "2 2 2\n")
        
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
'''
    
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
